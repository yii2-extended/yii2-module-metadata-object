# yii2-extended/yii2-module-metadata-object

This library is a simple implementation of the yii2-extended/yii2-module-metadata-interface.

![coverage](https://gitlab.com/yii2-extended/yii2-module-metadata-object/badges/main/pipeline.svg?style=flat-square) 
![build status](https://gitlab.com/yii2-extended/yii2-module-metadata-object/badges/main/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar yii2-extended/yii2-module-metadata-object ^8`


## Basic Usage

This library is to be used by implementing the `\Yii2Extended\Metadata\ModuleInterface` on `\yii\base\Module` objects.
It is also recommanded that the module uses the `\Yii2Extended\Metadata\ModuleTrait` that provides default methods on for
this interface.

Then use in the implementations the `\Yii2Extended\Metadata\Bundle` and
`\Yii2Extended\Metadata\Record` classes for each class that should be
available on the crud menus.


## License

MIT (See [license file](LICENSE)).
