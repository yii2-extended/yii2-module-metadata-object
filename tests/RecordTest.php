<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use Yii2Extended\Metadata\Record;
use Yii2Extended\Metadata\RecordInterface;

/**
 * RecordTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Extended\Metadata\Record
 *
 * @internal
 *
 * @small
 */
class RecordTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Record
	 */
	protected Record $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('class', $this->_object->__toString());
	}
	
	public function testGetClass() : void
	{
		$this->assertEquals('class', $this->_object->getClass());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('id', $this->_object->getId());
	}
	
	public function testGetTranslatedLabel() : void
	{
		$this->_object->setTLabel('yii', 'label');
		$this->assertEquals('label', $this->_object->getLabel());
	}
	
	public function testGetLabel() : void
	{
		$this->assertEquals('label', $this->_object->getLabel());
	}
	
	public function testGetBootstrapIconName() : void
	{
		$this->assertEquals('icon', $this->_object->getBootstrapIconName());
	}
	
	public function testGetIndex() : void
	{
		$this->_object->enableIndex();
		$this->assertTrue($this->_object->hasIndex());
		$this->_object->disableIndex();
		$this->assertFalse($this->_object->hasIndex());
	}
	
	public function testGetView() : void
	{
		$this->_object->enableView();
		$this->assertTrue($this->_object->hasView());
		$this->_object->disableView();
		$this->assertFalse($this->_object->hasView());
	}
	
	public function testGetCreate() : void
	{
		$this->_object->enableCreate();
		$this->assertTrue($this->_object->hasCreate());
		$this->_object->disableCreate();
		$this->assertFalse($this->_object->hasCreate());
	}
	
	public function testGetUpdate() : void
	{
		$this->_object->enableUpdate();
		$this->assertTrue($this->_object->hasUpdate());
		$this->_object->disableUpdate();
		$this->assertFalse($this->_object->hasUpdate());
	}
	
	public function testGetDelete() : void
	{
		$this->_object->enableDelete();
		$this->assertTrue($this->_object->hasDelete());
		$this->_object->disableDelete();
		$this->assertFalse($this->_object->hasDelete());
	}
	
	public function testGetSearch() : void
	{
		$this->_object->enableSearch();
		$this->assertTrue($this->_object->hasSearch());
		$this->_object->disableSearch();
		$this->assertFalse($this->_object->hasSearch());
	}
	
	public function testEnableReadOnly() : void
	{
		$this->_object->enableReadOnly();
		$this->assertTrue($this->_object->hasIndex());
		$this->assertTrue($this->_object->hasView());
		$this->assertFalse($this->_object->hasCreate());
		$this->assertFalse($this->_object->hasUpdate());
		$this->assertFalse($this->_object->hasDelete());
		$this->assertTrue($this->_object->hasSearch());
	}
	
	public function testEnableModification() : void
	{
		$this->_object->enableModification();
		$this->assertTrue($this->_object->hasIndex());
		$this->assertTrue($this->_object->hasView());
		$this->assertTrue($this->_object->hasCreate());
		$this->assertTrue($this->_object->hasUpdate());
		$this->assertFalse($this->_object->hasDelete());
		$this->assertTrue($this->_object->hasSearch());
	}
	
	public function testEnableFullAccess() : void
	{
		$this->_object->enableFullAccess();
		$this->assertTrue($this->_object->hasIndex());
		$this->assertTrue($this->_object->hasView());
		$this->assertTrue($this->_object->hasCreate());
		$this->assertTrue($this->_object->hasUpdate());
		$this->assertTrue($this->_object->hasDelete());
		$this->assertTrue($this->_object->hasSearch());
	}
	
	public function testIsAccessNull() : void
	{
		$this->assertFalse($this->_object->isAllowed(null));
	}
	
	public function testIsAccessNonExistant() : void
	{
		$this->assertFalse($this->_object->isAllowed('nonexistant'));
	}
	
	public function testIsAccessValid() : void
	{
		$this->assertTrue($this->_object->isAllowed(RecordInterface::ACTION_INDEX));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Record('class', 'id', 'label', 'icon', [RecordInterface::ACTION_INDEX, RecordInterface::ACTION_CREATE => true]);
	}
	
}
