<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use yii\base\Module;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\ModuleTrait;

/**
 * ModuleTraitTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Extended\Metadata\ModuleTrait
 *
 * @internal
 *
 * @small
 */
class ModuleTraitTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ModuleInterface
	 */
	protected ModuleInterface $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('id', $this->_object->getId());
	}
	
	public function testGetBootstrapIconName() : void
	{
		$this->assertEquals('view-stacked', $this->_object->getBootstrapIconName());
	}
	
	public function testGetLabel() : void
	{
		$this->assertStringContainsString('ModuleTrait', $this->_object->getLabel());
	}
	
	public function testGetEnabledBundles() : void
	{
		$this->assertEquals([], $this->_object->getEnabledBundles());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new class('id') extends Module implements ModuleInterface
		{
			
			use ModuleTrait
			{
				getId as traitId;
				getBootstrapIconName as traitBootstrapIconName;
				getLabel as traitLabel;
				getBundles as traitGetBundles;
				getEnabledBundles as traitEnabledBundles;
				isAllowed as traitIsAllowed;
			}
			
			public function getId() : string
			{
				return $this->traitId();
			}
			
			public function getBundles() : array
			{
				return $this->traitGetBundles();
			}
			
			public function getBootstrapIconName() : string
			{
				return $this->traitBootstrapIconName();
			}
			
			public function getLabel() : string
			{
				return $this->traitLabel();
			}
			
			public function getEnabledBundles() : array
			{
				return $this->traitEnabledBundles();
			}
			
			public function isAllowed(?string $bundleId, ?string $recordId, ?string $actionId) : bool
			{
				return $this->traitIsAllowed($bundleId, $recordId, $actionId);
			}
			
			public function __toString() : string
			{
				return __CLASS__;
			}
			
		};
	}
	
}
