<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Extended\Metadata\RecordInterface;

/**
 * BundleTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Extended\Metadata\Bundle
 *
 * @internal
 *
 * @small
 */
class BundleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Bundle
	 */
	protected Bundle $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('label', $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->_object->setId('id');
		$this->assertEquals('id', $this->_object->getId());
	}
	
	public function testGetTranslatedLabel() : void
	{
		$this->_object->setTLabel('yii', 'label');
		$this->assertEquals('label', $this->_object->getLabel());
	}
	
	public function testGetLabel() : void
	{
		$this->assertEquals('label', $this->_object->getLabel());
	}
	
	public function testGetEnabledRecords() : void
	{
		$expected = [
			'record' => new Record('class', 'record', 'label', 'icon', [RecordInterface::ACTION_INDEX, RecordInterface::ACTION_CREATE => true]),
		];
		
		$this->assertEquals($expected, $this->_object->getEnabledRecords());
	}
	
	public function testIsAllowedRecordNull() : void
	{
		$this->assertFalse($this->_object->isAllowed(null, null));
	}
	
	public function testIsAllowedNonExistant() : void
	{
		$this->assertFalse($this->_object->isAllowed('nxrecord', 'action'));
	}
	
	public function testIsAllowedExistant() : void
	{
		$this->assertTrue($this->_object->isAllowed('record', RecordInterface::ACTION_INDEX));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Bundle('label', [
			'record' => new Record('class', 'id', 'label', 'icon', [RecordInterface::ACTION_INDEX, RecordInterface::ACTION_CREATE => true]),
		]);
	}
	
}
