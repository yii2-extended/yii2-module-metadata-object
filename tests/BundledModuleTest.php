<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use Yii2Extended\Metadata\BundledModule;

/**
 * BundledModuleTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Extended\Metadata\BundledModule
 * @internal
 * @small
 */
class BundledModuleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BundledModule
	 */
	protected BundledModule $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BundledModule('__id');
	}
	
}
