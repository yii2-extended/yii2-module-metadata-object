<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use yii\base\Module;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\ModuleTrait;
use Yii2Extended\Metadata\Record;
use Yii2Extended\Metadata\RecordInterface;

/**
 * ModuleTraitInheritedTest test file.
 *
 * @author Anastaszor
 * @covers \Yii2Extended\Metadata\ModuleTrait
 *
 * @internal
 *
 * @small
 */
class ModuleTraitInheritedTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var ModuleInterface
	 */
	protected ModuleInterface $_object;
	
	public function testIsAllowedNull() : void
	{
		$this->assertFalse($this->_object->isAllowed(null, null, null));
	}
	
	public function testIsNonExistant() : void
	{
		$this->assertFalse($this->_object->isAllowed('nxbundle', 'class', 'action'));
	}
	
	public function testSuccess() : void
	{
		$this->assertTrue($this->_object->isAllowed('bundle', 'record', RecordInterface::ACTION_INDEX));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new class('id') extends Module implements ModuleInterface
		{
			
			use ModuleTrait
			{
				getId as traitId;
				getBootstrapIconName as traitBootstrapIconName;
				getBundles as traitGetBundles;
				getLabel as traitLabel;
				getEnabledBundles as traitEnabledBundles;
				isAllowed as traitIsAllowed;
			}
			
			public function getId() : string
			{
				return $this->traitId();
			}
			
			public function getBundles() : array
			{
				return $this->traitGetBundles();
			}
			
			public function getBootstrapIconName() : string
			{
				return $this->traitBootstrapIconName();
			}
			
			public function getLabel() : string
			{
				return $this->traitLabel();
			}
			
			public function isAllowed(?string $bundleId, ?string $recordId, ?string $actionId) : bool
			{
				return $this->traitIsAllowed($bundleId, $recordId, $actionId);
			}
			
			public function __toString() : string
			{
				return __CLASS__;
			}
			
			public function getEnabledBundles() : array
			{
				return [
					'bundle' => new Bundle('label', [
						'record' => new Record('class', 'id', 'label', 'icon', [RecordInterface::ACTION_INDEX, RecordInterface::ACTION_CREATE => true]),
					]),
				];
			}
			
		};
	}
	
}
