<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Metadata;

use yii\BaseYii;

/**
 * Bundle class file.
 * 
 * This is a simple implementation of the BundleInterface.
 * 
 * @author Anastaszor
 */
class Bundle implements BundleInterface
{
	
	/**
	 * The id of this bundle.
	 * 
	 * @var ?string
	 */
	protected ?string $_id = null;
	
	/**
	 * The label of the bundle.
	 * 
	 * @var ?string
	 */
	protected ?string $_label = null;
	
	/**
	 * The records of this bundle.
	 * 
	 * @var array<string, RecordInterface>
	 */
	protected array $_records = [];
	
	/**
	 * Builds a new Bundle with the given label and records.
	 * 
	 * @param ?string $label
	 * @param array<string, RecordInterface> $records
	 */
	public function __construct(?string $label = null, array $records = [])
	{
		$this->setLabel($label);
		$this->setRecords($records);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_label ?? $this->getId();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\BundleInterface::setId()
	 */
	public function setId(?string $id) : static
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\BundleInterface::getId()
	 */
	public function getId() : string
	{
		return $this->_id ?? \sha1(\implode('|', \array_keys($this->_records)));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\BundleInterface::setTLabel()
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function setTLabel(string $category, string $message, array $params = [], ?string $language = null) : static
	{
		return $this->setLabel(BaseYii::t($category, $message, $params, $language));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\BundleInterface::setLabel()
	 */
	public function setLabel(?string $label) : static
	{
		$this->_label = $label;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\BundleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return $this->_label ?? '(Undefined)';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\BundleInterface::setRecords()
	 */
	public function setRecords(array $records) : static
	{
		/** @var Record $value */
		foreach($records as $key => $value)
		{
			$this->_records[(string) $key] = \is_string($key) ? $value->setId($key) : $value;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\BundleInterface::getEnabledRecords()
	 */
	public function getEnabledRecords() : array
	{
		return $this->_records;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\BundleInterface::isAllowed()
	 */
	public function isAllowed(?string $recordId, ?string $actionId) : bool
	{
		if(null === $recordId || null === $actionId)
		{
			return false;
		}
		
		if(!isset($this->_records[$recordId]))
		{
			return false;
		}
		
		return $this->_records[$recordId]->isAllowed($actionId);
	}
	
}
