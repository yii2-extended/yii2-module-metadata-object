<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Metadata;

/**
 * ModuleTrait trait file.
 * 
 * This trait is to be implemented by a module that uses the ModuleInterface.
 * 
 * @author Anastaszor
 */
trait ModuleTrait
{
	
	/**
	 * The bundles of this module, if defined.
	 * 
	 * @var array<string, BundleInterface>
	 */
	protected array $_cachedBundles = [];
	
	/**
	 * Gets the id of the module.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'view-stacked';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return \basename(\str_replace('\\', '/', __CLASS__));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBundles()
	 * @return array<string, BundleInterface>
	 */
	public function getBundles() : array
	{
		if([] === $this->_cachedBundles)
		{
			$bundles = [];
			
			foreach($this->getEnabledBundles() as $key => $bundle)
			{
				$bundles[(string) $key] = $bundle->setId((string) $key);
			}
			
			$this->_cachedBundles = $bundles;
		}
		
		return $this->_cachedBundles;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 * @return array<string, BundleInterface>
	 */
	public function getEnabledBundles() : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::isAllowed()
	 */
	public function isAllowed(?string $bundleId, ?string $recordId, ?string $actionId) : bool
	{
		if(null === $bundleId || null === $recordId || null === $actionId)
		{
			return false;
		}
		
		$bundles = $this->getBundles();
		
		if(!isset($bundles[$bundleId]))
		{
			return false;
		}
		
		return $bundles[$bundleId]->isAllowed($recordId, $actionId);
	}
	
}
