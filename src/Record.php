<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Metadata;

use yii\BaseYii;
use yii\db\ActiveRecordInterface;
use yii\db\TableSchema;
use yii\helpers\Inflector;

/**
 * Record class file.
 * 
 * This class is a simple implementation of the RecordInterface.
 * 
 * @author Anastaszor
 */
class Record implements RecordInterface
{
	
	/**
	 * The identifier of the class.
	 *
	 * @var ?string
	 */
	protected ?string $_id = null;
	
	/**
	 * The class on which this metadata applies to.
	 *
	 * @var class-string<ActiveRecordInterface>
	 */
	protected string $_class;
	
	/**
	 * The label that represents the class.
	 *
	 * @var ?string
	 */
	protected ?string $_label = null;
	
	/**
	 * The bootstrap icon name of the record.
	 *
	 * @var ?string
	 */
	protected ?string $_bootstrapIconName = null;
	
	/**
	 * The actions that are available on this class.
	 *
	 * @var array{'index': bool, 'view': bool, 'create': bool, 'update': bool, 'delete': bool, 'search': bool}
	 */
	protected array $_actions = [
		self::ACTION_INDEX => true,
		self::ACTION_VIEW => true,
		self::ACTION_CREATE => false,
		self::ACTION_UPDATE => false,
		self::ACTION_DELETE => false,
		self::ACTION_SEARCH => true,
	];
	
	/**
	 * Builds a new record metadata with the class and the allowed actions.
	 * Accepted actions are self::ACTION_INDEX, self::ACTION_VIEW,
	 * self::ACTION_CREATE, self::ACTION_UPDATE, self::ACTION_DELETE and
	 * self::ACTION_SEARCH.
	 *
	 * @param class-string<ActiveRecordInterface> $class
	 * @param ?string $id
	 * @param ?string $label
	 * @param ?string $bootstrapIconName
	 * @param array<integer, string>|array<string, boolean> $actions
	 */
	public function __construct(string $class, ?string $id = null, ?string $label = null, ?string $bootstrapIconName = null, array $actions = [])
	{
		$this->_class = $class;
		$this->setId($id);
		$this->setLabel($label);
		$this->setBootstrapIconName($bootstrapIconName);
		$this->setActions($actions);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getClass();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::setId()
	 */
	public function setId(?string $id) : static
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::getId()
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function getId() : string
	{
		return $this->_id ?? Inflector::camel2id(\basename($this->_class));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::getClass()
	 */
	public function getClass() : string
	{
		return $this->_class;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::setLabel()
	 */
	public function setLabel(?string $label) : static
	{
		$this->_label = $label;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::setTLabel()
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function setTLabel(string $category, string $message, array $params = [], ?string $language = null) : static
	{
		return $this->setLabel(BaseYii::t($category, $message, $params, $language));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return $this->_label ?? \basename($this->_class);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::setBootstrapIconName()
	 */
	public function setBootstrapIconName(?string $icon) : static
	{
		$this->_bootstrapIconName = $icon;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return $this->_bootstrapIconName ?? 'grid-1x2';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::getFieldNames()
	 * @psalm-suppress MixedInferredReturnType
	 */
	public function getFieldNames() : array
	{
		/** @var TableSchema $tableSchema */
		/** @phpstan-ignore-next-line */ /** @psalm-suppress UndefinedMethod */
		$tableSchema = $this->_class::getTableSchema();

		/** @psalm-suppress MixedMethodCall,MixedReturnStatement */
		return $tableSchema->getColumnNames();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::setActions()
	 */
	public function setActions(array $actions) : static
	{
		foreach($actions as $key => $value)
		{
			if(\is_string($key))
			{
				$key = (string) \mb_strtolower($key);
				if(isset($this->_actions[$key]))
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion */
					$this->_actions[$key] = (bool) $value;
				}
			}
			
			if(\is_string($value))
			{
				$key = (string) \mb_strtolower($value);
				if(isset($this->_actions[$key]))
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion */
					$this->_actions[$key] = true;
				}
			}
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::enableIndex()
	 */
	public function enableIndex() : static
	{
		$this->_actions[self::ACTION_INDEX] = true;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::disableIndex()
	 */
	public function disableIndex() : static
	{
		$this->_actions[self::ACTION_INDEX] = false;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::hasIndex()
	 */
	public function hasIndex() : bool
	{
		return $this->_actions[self::ACTION_INDEX];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::enableView()
	 */
	public function enableView() : static
	{
		$this->_actions[self::ACTION_VIEW] = true;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::disableView()
	 */
	public function disableView() : static
	{
		$this->_actions[self::ACTION_VIEW] = false;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::hasView()
	 */
	public function hasView() : bool
	{
		return $this->_actions[self::ACTION_VIEW];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::enableCreate()
	 */
	public function enableCreate() : static
	{
		$this->_actions[self::ACTION_CREATE] = true;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::disableCreate()
	 */
	public function disableCreate() : static
	{
		$this->_actions[self::ACTION_CREATE] = false;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::hasCreate()
	 */
	public function hasCreate() : bool
	{
		return $this->_actions[self::ACTION_CREATE];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::enableUpdate()
	 */
	public function enableUpdate() : static
	{
		$this->_actions[self::ACTION_UPDATE] = true;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::disableUpdate()
	 */
	public function disableUpdate() : static
	{
		$this->_actions[self::ACTION_UPDATE] = false;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::hasUpdate()
	 */
	public function hasUpdate() : bool
	{
		return $this->_actions[self::ACTION_UPDATE];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::enableDelete()
	 */
	public function enableDelete() : static
	{
		$this->_actions[self::ACTION_DELETE] = true;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::disableDelete()
	 */
	public function disableDelete() : static
	{
		$this->_actions[self::ACTION_DELETE] = false;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::hasDelete()
	 */
	public function hasDelete() : bool
	{
		return $this->_actions[self::ACTION_DELETE];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::enableSearch()
	 */
	public function enableSearch() : static
	{
		$this->_actions[self::ACTION_SEARCH] = true;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::disableSearch()
	 */
	public function disableSearch() : static
	{
		$this->_actions[self::ACTION_SEARCH] = false;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::hasSearch()
	 */
	public function hasSearch() : bool
	{
		return $this->_actions[self::ACTION_SEARCH];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::enableReadOnly()
	 */
	public function enableReadOnly() : static
	{
		$this->_actions = [
			self::ACTION_INDEX => true,
			self::ACTION_VIEW => true,
			self::ACTION_CREATE => false,
			self::ACTION_UPDATE => false,
			self::ACTION_DELETE => false,
			self::ACTION_SEARCH => true,
		];
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::enableModification()
	 */
	public function enableModification() : static
	{
		$this->_actions = [
			self::ACTION_INDEX => true,
			self::ACTION_VIEW => true,
			self::ACTION_CREATE => true,
			self::ACTION_UPDATE => true,
			self::ACTION_DELETE => false,
			self::ACTION_SEARCH => true,
		];
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::enableFullAccess()
	 */
	public function enableFullAccess() : static
	{
		$this->_actions = [
			self::ACTION_INDEX => true,
			self::ACTION_VIEW => true,
			self::ACTION_CREATE => true,
			self::ACTION_UPDATE => true,
			self::ACTION_DELETE => true,
			self::ACTION_SEARCH => true,
		];
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\RecordInterface::isAllowed()
	 */
	public function isAllowed(?string $actionId) : bool
	{
		if(null === $actionId)
		{
			return false;
		}
		
		if(!isset($this->_actions[$actionId]))
		{
			return false;
		}
		
		return $this->_actions[$actionId];
	}
	
}
